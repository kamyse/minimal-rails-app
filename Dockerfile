FROM ruby:2.3

ADD . /app

WORKDIR /app

RUN apt update
RUN apt install -y nodejs 

RUN bundle install


ENTRYPOINT ["bin/rails", "server"]
